@extends('layout')
@section('css')
<link rel='stylesheet' href='https://cdn.jsdelivr.net/npm/sweetalert2@7.12.15/dist/sweetalert2.min.css'>
<style>
.container {
    color: white;
    text-align: center;
    border-radius: 100px 0 0 100px;
}

.section {
    display: grid;
    width: 100%;
    height: 100%;
    grid-template-columns: 1fr;
    grid-template-rows: 100vh;
    /* adjusted */
}

#selects {
    margin-top: 10px;
    margin-left: 270px;

}

.container {
    text-align: center;
    border-radius: 100px 0 0 100px;
}


label {

    border: 3px white solid;
    padding: 20px;
    width: 250px;
    float: left;
    font-size: 1.5em;
    cursor: pointer;
    color: black;
}

label {
    border: 3px white solid;
    background: white;
    padding: 20px;
    width: 250px;
    float: left;
    font-size: 1.5em;
    cursor: pointer;
}

.section input[type="radio"] {
    background: black;
    display: none;
}

input[type="radio"]:checked+label {
    background: black;
    color: #fff;
}

h1 {
    color: black;
}
</style>
@endsection
@section('content')
<section class="content">
    <div class="container-fluid">
        <!-- Small boxes (Stat box) -->
        <div class="row">
            <div class="col-lg-3 col-6">
                <!-- small box -->
                <div class="small-box bg-info">
                    <div class="inner">
                        <h3>{{$mahasiswa->count()}}</h3>
                        <p>Total Mahasiswa</p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-bag"></i>
                    </div>
                </div>
            </div>
            <!-- ./col -->
            <div class="col-lg-3 col-6">
                <!-- small box -->
                <div class="small-box bg-success">
                    <div class="inner">
                        <h3>{{"Rp. " . number_format($total)}}</h3>
                        <p>Total </p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-stats-bars"></i>
                    </div>
                </div>
            </div>
            <!-- ./col -->
            <div class="col-lg-3 col-6">
                <!-- small box -->
                <div class="small-box bg-warning">
                    <div class="inner">
                        <h3>44</h3>
                        <p>User Registrations</p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-person-add"></i>
                    </div>
                </div>
            </div>
            <!-- ./col -->
            <div class="col-lg-3 col-6">
                <!-- small box -->
                <div class="small-box bg-danger">
                    <div class="i
                    nner">
                        <h3>65</h3>
                        <p>Unique Visitors</p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-pie-graph"></i>
                    </div>
                </div>
            </div>
            <!-- ./col -->
        </div>
        <div class='section'>
            <div class='container'>
                <div class="row">
                    <div class="col-md-12">
                        <h1>Pilih Semester</h1>
                    </div>
                </div>
                <div class="row" id="selects">
                    <input type="radio" alt="camara" name="casa" id="camara" value="0"
                        {{ ($data->sms == 0)?"checked":""}}>
                    <label class="camara" for="camara">Ganjil</label>
                    <input type="radio" alt="senado" name="casa" id="senado" value="1"
                        {{ ($data->sms == 1)?"checked":""}}>
                    <label class="senado" for="senado">Genap</label>
                </div>

            </div>
        </div>

    </div><!-- /.container-fluid -->
</section>
@endsection
@section('js')
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@7.12.15/dist/sweetalert2.all.min.js"></script>
<script>
$('input[type=radio][name=casa]').change(function() {
    if (this.value == 1) {
        swal({
            title: 'Apakah anda ingin pindah ke semester genap?',
            showCancelButton: true,
            confirmButtonText: 'YA',
            cancelButtonText: 'TIDAK'
        }).then((result) => {
            if (result.dismiss === swal.DismissReason.cancel) {
                swal(
                    'di batalkan',
                    '',
                    'error'
                );
                location.reload();
            } else {
                $.get('sms/'+ 1, function(data) {
                    console.log(data);
                    swal(
                        'Semester genap telah di pilih',
                        '',
                        'success'
                    )
                })
            }
        })

    } else if (this.value == 0) {
        swal({
            title: 'Apakah anda ingin pindah ke semester ganjil?',
            showCancelButton: true,
            confirmButtonText: 'YA',
            cancelButtonText: 'TIDAK'
        }).then((result) => {
            if (result.dismiss === swal.DismissReason.cancel) {
                swal(
                    'di batalkan',
                    '',
                    'error'
                );
                location.reload();
            } else {
                $.get('sms/'+ 0, function(data) {
                    console.log(data);
                    swal(
                        'Semester ganjil telah di pilih',
                        '',
                        'success'
                    )
                })


            }
        })
    }
});
</script>
@endsection
