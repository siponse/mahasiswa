@extends('layout')
@section('content')
<!-- Main content -->
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <section class="content">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-12">
                                <div class="card">
                                    <div class="card-header">
                                        <h3 class="card-title">Data beasiswa</h3>
                                    </div>
                                    <!-- /.card-header -->
                                    <div class="card-body">
                                        <button type="button" class="btn btn-info" data-toggle="modal"
                                            data-target="#modal-lg">
                                            Tambah beasiswa
                                        </button>
                                        <div class="modal fade" id="modal-lg">
                                            <div class="modal-dialog modal-lg">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h4 class="modal-title">Tambah beasiswa</h4>
                                                        <button type="button" class="close" data-dismiss="modal"
                                                            aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                    <div class="modal-body">
                                                        <form action="{{route('beasiswa.store')}}" method="post" id="tmbh">
                                                            @csrf
                                                            <div class="card-body">
                                                                <div class="form-group">
                                                                    <label for="exampleInput1">Nama beasiswa</label>
                                                                    <input type="text" name="nama" class="form-control"
                                                                        id="exampleInput1" placeholder="nama_beasiswa" required>
                                                                </div>
                                                                <div class="form-group">
                                                                    <label for="exampleInputPassword1">jumlah</label>
                                                                    <input type="text" name="jumlah"
                                                                        class="form-control" id="exampleInputPassword1"
                                                                        placeholder="NIM" required>
                                                                </div>
                                                                <div class="form-group">
                                                                    <label for="exampleInputPassword1">masa
                                                                        beasiswa</label>
                                                                    <input type="text" name="masa" class="form-control"
                                                                        id="exampleInputPassword1" placeholder="NIM" required>
                                                                </div>
                                                            </div>
                                                            <!-- /.card-body -->
                                                            <div class="modal-footer justify-content-between">
                                                                <button type="button" class="btn btn-default"
                                                                    data-dismiss="modal">Close</button>
                                                                <button type="submit" class="btn btn-primary">Save
                                                                    changes</button>
                                                            </div>
                                                        </form>
                                                    </div>

                                                </div>
                                                <!-- /.modal-content -->
                                            </div>
                                            <!-- /.modal-dialog -->
                                        </div>
                                        <table class="table table-striped table-bordered table-sm" id="table-beasiswa">
                                            <thead>
                                                <tr>
                                                    <th>No</th>
                                                    <th>Nama beasiswa</th>
                                                    <th>Jumlah beasiswa</th>
                                                    <th>masa beasiswa</th>
                                                    <th>Action</th>
                                                </tr>
                                            </thead>
                                        </table>
                                    </div>
                                    <!-- /.card-body -->
                                </div>
                                <!-- /.card -->
                            </div>
                            <!-- /.col -->
                        </div>
                        <!-- /.row -->
                    </div>
                    <!-- /.container-fluid -->
                </section>

            </div>
            <!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <div class="modal fade" id="modal-edit-beasiswa">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Edit Data beasiswa</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="{{route('beasiswa.edit')}}" method="post">
                        @csrf

                        <div class="card-body">
                            <div class="form-group">
                                <label for="exampleInput1">Nama beasiswa</label>
                                <input type="text" id="nama" name="nama" class="form-control" id="exampleInput1"
                                    placeholder="nama_beasiswa">
                            </div>
                            <div class="form-group">
                                <label for="exampleInputPassword1">jumlah</label>
                                <input type="text" id="jumlah" name="jumlah" class="form-control" id="exampleInputPassword1"
                                    placeholder="NIM">
                            </div>
                            <div class="form-group">
                                <label for="exampleInputPassword1">masa beasiswa</label>
                                <input type="text" id="masa" name="masa" class="form-control" id="exampleInputPassword1"
                                    placeholder="NIM">
                            </div>
                            <input type="hidden" name="id" id="id">
                        </div>
                        <!-- /.card-body -->
                        <div class="modal-footer justify-content-between">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary">Save
                                changes</button>
                        </div>
                    </form>
                </div>

            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <div class="modal fade" tabindex="-1" role="dialog" id="konfirmasi-modal" data-backdrop="false">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">PERHATIAN</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <p><b>Jika menghapus beasiswa maka</b></p>
                    <p>*data beasiswa tersebut hilang selamanya, apakah anda yakin?</p>
                </div>
                <div class="modal-footer bg-whitesmoke br">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-danger" name="tombol-hapus" id="tombol-hapus">Hapus
                        Data</button>
                </div>
            </div>
        </div>
    </div>
    @endsection

    @section('js')
    <script type="text/javascript">
    
    $('#tmbh').validate();
    $(function() {
        var oTable = $('#table-beasiswa').DataTable({

            serverSide: true,
            ajax: {
                url: '{{ url("beasiswa") }}'
            },
            columns: [{
                    data: 'DT_RowIndex',
                    name: 'DT_RowIndex'
                },
                {
                    data: 'nama_beasiswa',
                    name: 'nama_beasiswa'
                },
                {
                    data: 'jumlah_beasiswa',
                    name: 'jumlah_beasiswa'
                },
                {
                    data: 'masa_beasiswa',
                    name: 'masa_beasiswa'
                },
                {
                    data: 'action',
                    name: 'action'
                },
            ],
        });
    });
    $('body').on('click', '.edit-post', function() {
        var data_id = $(this).data('id');
        $.get('beasiswa/' + 'update/' + data_id, function(data) {
            $('#modal-edit-beasiswa').modal('show');
            //set value masing-masing id berdasarkan data yg diperoleh dari ajax get request diatas               
            $('#id').val(data.id);
            $('#nama').val(data.nama_beasiswa);
            $('#jumlah').val(data.jumlah_beasiswa);
            $('#masa').val(data.masa_beasiswa);
        })
    });

    $(document).on('click', '.delete', function() {
        dataId = $(this).attr('id');
        $('#konfirmasi-modal').modal('show');
    });
    $('#tombol-hapus').click(function() {
        $.ajax({
            url: "beasiswa/delete/" + dataId, //eksekusi ajax ke url ini
            type: 'get',
            beforeSend: function() {
                $('#tombol-hapus').text('Hapus Data'); //set text untuk tombol hapus
            },
            success: function(data) { //jika sukses

                $('#konfirmasi-modal').modal('hide');
                location.reload();
            }
        })
    });
    </script>
    @endsection