@extends('layout')
@section('css')
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
@endsection
@section('content')
<!-- Main content -->
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <section class="content">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-12">
                                <div class="card">
                                    <div class="card-header">
                                        <h3 class="card-title">daftar pembayaran</h3>
                                    </div>
                                    <!-- /.card-header -->
                                    <div class="card-body">
                                        <button type="button" class="btn btn-info" data-toggle="modal"
                                            data-target="#modal-lg">
                                            Tambah daftar pembayaran
                                        </button>
                                        <div class="modal fade" id="modal-lg">
                                            <div class="modal-dialog modal-lg">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h4 class="modal-title">Tambah daftar pembayaran</h4>
                                                        <button type="button" class="close" data-dismiss="modal"
                                                            aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                    <div class="modal-body">
                                                        <form action="{{route('pembayaranmahasiswa.store')}}"
                                                            method="post" id="tmbh">
                                                            @csrf
                                                            <div class="card-body">
                                                                <div id="myModal" class="modal fade" tabindex="-1"
                                                                    role="dialog" aria-hidden="true">
                                                                    ...
                                                                    <select id="mySelect2">
                                                                        ...
                                                                    </select>
                                                                    ...
                                                                </div>
                                                                <div class="form-group">
                                                                    <label for="exampleInput1">Nama Mahasiswa</label>
                                                                    <div class="row">
                                                                        <div class="col-md-12">
                                                                            <select id="paket" name="mahasiswa[]"
                                                                                class="form-control" multiple="multiple"
                                                                                style="width:100%;color:black;">
                                                                                @foreach($mahasiswa as $item)
                                                                                <option value="{{$item->id}}">
                                                                                    {{$item->Nama}}( {{$item->VA}} )
                                                                                </option>
                                                                                @endforeach
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group">
                                                                    <label for="exampleInputPassword1">Daftar
                                                                        pembayaran</label>
                                                                    <div class="row">
                                                                        <div class="col-md-12">
                                                                            <select id="paket1" name="rincian[]"
                                                                                class="form-control cek"
                                                                                multiple="multiple"
                                                                                style="width:100%;color:black;">
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group">
                                                                    <label for="exampleInput1">total pembayaran</label>
                                                                    <input type="text" name="Total" class="form-control"
                                                                        id="biaya123" readonly>
                                                                </div>
                                                                <div class="form-group">
                                                                    <label for="exampleInput1">jumlah yang di
                                                                        bayar</label>
                                                                    <input type="text" name="biaya" class="form-control"
                                                                        id="exampleInput1" placeholder="nama_pembayaran"
                                                                        required>
                                                                </div>
                                                            </div>
                                                            <!-- /.card-body -->
                                                            <div class="modal-footer justify-content-between">
                                                                <button type="button" class="btn btn-default"
                                                                    data-dismiss="modal">Close</button>
                                                                <button type="submit" class="btn btn-primary">Save
                                                                    changes</button>
                                                            </div>
                                                        </form>
                                                    </div>

                                                </div>
                                                <!-- /.modal-content -->
                                            </div>
                                            <!-- /.modal-dialog -->
                                        </div>
                                        <table class="table table-striped table-bordered table-sm"
                                            id="table-pembayaran">
                                            <thead>
                                                <tr>
                                                    <th>No</th>
                                                    <th>Nama</th>
                                                    <th>VA</th>
                                                    <th>Prodi</th>
                                                    <th>Semester</th>
                                                    <th>Keterangan</th>
                                                    <th>Action</th>
                                                </tr>
                                            </thead>
                                        </table>
                                    </div>
                                    <!-- /.card-body -->
                                </div>
                                <!-- /.card -->
                            </div>
                            <!-- /.col -->
                        </div>
                        <!-- /.row -->
                    </div>
                    <!-- /.container-fluid -->
                </section>

            </div>
            <!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <div class="modal fade" id="modal-edit-pembayaran">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Edit Data pembayaran</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="{{route('pembayaranmahasiswa.edit')}}" method="post">
                        @csrf
                        <div class="card-body">
                            <div class="form-group">
                                <label for="exampleInput1">Nama pembayaran</label>
                                <input type="text" name="nama" class="form-control" id="nama"
                                    placeholder="nama_pembayaran">
                            </div>
                            <div class="form-group">
                                <label for="exampleInputPassword1">Prodi</label>
                                <select id="prodi" class="form-control" name="prodi">
                                    @foreach($prodi as $item)
                                    <option value="{{$item->id}}">
                                        {{$item->nama_prodi}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="exampleInput1">Angkatan</label>
                                <input type="text" name="Angkatan" class="form-control" id="angkatan"
                                    placeholder="nama_pembayaran">
                            </div>
                            <div class="form-group">
                                <label for="exampleInput1">Biaya</label>
                                <input type="text" id="biaya123" name="biaya" class="form-control" id="biaya"
                                    placeholder="nama_pembayaran">
                            </div>
                            <input type="hidden" name="id" id="id">
                        </div>
                        <!-- /.card-body -->
                        <div class="modal-footer justify-content-between">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary">Save
                                changes</button>
                        </div>
                    </form>
                </div>

            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <div class="modal fade" id="modal-detail-pembayaran">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Detail Pembayaran</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="{{route('pembayaranmahasiswa.edit')}}" method="post">
                        @csrf
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-2"><label>Nama</label></div>
                                <div class="col-md-1"> : </div>
                                <div class="col-md-9">
                                    <p id="nama123">Muhammad fahrul akbar</p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-2"><label>NIM</label></div>
                                <div class="col-md-1"> : </div>
                                <div class="col-md-9">
                                    <p id="VA123">16650071</p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-2"><label>Prodi</label></div>
                                <div class="col-md-1"> : </div>
                                <div class="col-md-9">
                                    <p id="prodi123">Teknik Informatika</p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-2"><label>Angkatan</label></div>
                                <div class="col-md-1"> : </div>
                                <div class="col-md-9">
                                    <p id="angkatan123">2020</p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-2"><label>Semester</label></div>
                                <div class="col-md-1"> : </div>
                                <div class="col-md-9">
                                    <p id="semester">2</p>
                                </div>
                            <div class="row">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th>Daftar Pembayaran</th>
                                            <th>Biaya</th>
                                            <th>Jumlah yang Dibayar</th>
                                            <th>Tanggal Pembayaran</th>
                                            <th>Status</th>
                                        </tr>
                                    </thead>

                                    <tbody id="isi">
                                        <tr>
                                            <td>UAS</td>
                                            <td>50000</td>
                                            <td>50000</td>
                                            <td>LUNAS</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>

                            <input type="hidden" name="id" id="id">
                        </div>
                        <!-- /.card-body -->
                        <div class="modal-footer justify-content-between">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary">Print Invoice</button>
                        </div>
                    </form>
                </div>

            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <div class="modal fade" tabindex="-1" role="dialog" id="konfirmasi-modal" data-backdrop="false">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">PERHATIAN</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <p><b>Jika menghapus pembayaran maka</b></p>
                    <p>*data pembayaran tersebut hilang selamanya, apakah anda yakin?</p>
                </div>
                <div class="modal-footer bg-whitesmoke br">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-danger" name="tombol-hapus" id="tombol-hapus">Hapus
                        Data</button>
                </div>
            </div>
        </div>
    </div>
    @endsection

    @section('js')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
    <script type="text/javascript">
    $('#tmbh').validate();
    $(function() {
        var oTable = $('#table-pembayaran').DataTable({
            serverSide: true,
            ajax: {
                url: '{{ url("pembayaranmahasiswa") }}'
            },
            columns: [{
                    data: 'DT_RowIndex',
                    name: 'DT_RowIndex'
                },
                {
                    data: 'Nama',
                    name: 'Nama'
                },
                {
                    data: 'VA',
                    name: 'VA'
                },
                {
                    data: 'nama_prodi',
                    name: 'nama_prodi'
                },
                {
                    data: 'smt',
                    name: 'Semester'
                },
                {
                    data: 'keterangan',
                    name: 'keterangan'
                },
                {
                    data: 'action',
                    name: 'action'
                },
            ],
        });
    });
    $('body').on('click', '.edit-post', function() {
        var data_id = $(this).data('id');
        $.get('pembayaran/' + 'update/' + data_id, function(data) {
            console.log(data);
            $('#modal-edit-pembayaran').modal('show');
            //set value masing-masing id berdasarkan data yg diperoleh dari ajax get request diatas               
            $('#id').val(data.id);
            $('#nama').val(data.daftar_pembayaran);
            $('#biaya').val(data.biaya);
            $('#angkatan').val(data.angkatan);
        })
    });
    $('body').on('click', '.detail-post', function() {

        var data_id = $(this).data('id');
        $.get('pembayaranmahasiswa/' + 'update/' + data_id, function(data) {
            console.log(data);
            $('#modal-detail-pembayaran').modal('show');
            //set value masing-masing id berdasarkan data yg diperoleh dari ajax get request diatas               
            $('#nama123').html(data[0].Nama);
            $('#VA123').html(data[0].VA);
            $('#prodi123').html(data[0].nama_prodi);
            $('#angkatan123').html(data.Angkatan);
            $('#isi').html(" ");
            var krng = data[0].total - data[0].jumlah
            if (krng == 0) {
                var lns = "lunas";
            } else {
                var lns = "belum lunas";
            }

            $('#bayar').html(" ");
            $.each(data, function(i, item) {
                var isi = `<tr>
                                        <td>` + data[i].daftar_pembayaran + `</td>
                                        <td>` + data[i].biaya + `</td>
                                        <td>` + data[i].biaya + `</td>
                                        <td>` + data[i].created_at + `</td>
                                        <td> LUNAS </td>
                                    </tr>`;
                $('#isi').append(isi);
                var tesi = data[i];
                console.log(tesi);
                var byr = `<li>` + data[i].daftar_pembayaran + `</li>`;
                $('#bayar').append(byr);
            });
        })
    });

    $(document).on('click', '.delete', function() {
        dataId = $(this).attr('id');
        $('#konfirmasi-modal').modal('show');
    });
    $('#tombol-hapus').click(function() {
        $.ajax({
            url: "pembayaranmahasiswa/delete/" + dataId, //eksekusi ajax ke url ini
            type: 'get',
            beforeSend: function() {
                $('#tombol-hapus').text('Hapus Data'); //set text untuk tombol hapus
            },
            success: function(data) { //jika sukses

                $('#konfirmasi-modal').modal('hide');
                location.reload();
            }
        })
    });

    $("#paket").select2({
        placeholder: "Silahkan Pilih",
    });

    $("#paket1").select2({
        placeholder: "Silahkan Pilih",
    })
    $('#paket').on('select2:select', function(e) {
        console.log('select event');
        var data = e.params.data;
        console.log(data);
        $.ajax({
            url: "data/daftar/" + data.id, //eksekusi ajax ke url ini
            type: 'get',
            success: function(data) { //jika sukses

                $.each(data, function(i, item) {
                    console.log(data[i].daftar_pembayaran);
                    tes = `<option value="">` + data[i].daftar_pembayaran + `</option>`;
                    $("#paket1").select2({
                        placeholder: "Silahkan pilih",
                        data: data
                    })
                });
                $('#paket1').on('select2:select', function(e) {
                    var dataId = $(this).val();
                    console.log(dataId);
                    var tes = 0;
                    $.ajax({
                        url: "pembayaranmahasiswa/showharga/" +
                            dataId, //eksekusi ajax ke url ini
                        type: 'get',
                        success: function(data) { //jika sukses
                            $.each(data, function(i, item) {
                                console.log(data);
                                tes += data[i].biaya;
                                $('#biaya123').val(tes);
                            });
                        }
                    })
                });
            }
        })
    });
    </script>
    @endsection