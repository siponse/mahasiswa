@extends('layout')
@section('content')
<!-- Main content -->
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <section class="content">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-12">
                                <div class="card">
                                    <div class="card-header">
                                        <h3 class="card-title">daftar pembayaran</h3>
                                    </div>
                                    <!-- /.card-header -->
                                    <div class="card-body">
                                        <button type="button" class="btn btn-info" data-toggle="modal"
                                            data-target="#modal-lg">
                                            Tambah daftar pembayaran
                                        </button>
                                        <div class="modal fade" id="modal-lg">
                                            <div class="modal-dialog modal-lg">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h4 class="modal-title">Tambah daftar pembayaran</h4>
                                                        <button type="button" class="close" data-dismiss="modal"
                                                            aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                    <div class="modal-body">
                                                        <form action="{{route('pembayaran.store')}}" method="post" id="tmbh">
                                                            @csrf
                                                            <div class="card-body">
                                                                <div class="form-group">
                                                                    <label for="exampleInput1">Nama pembayaran</label>
                                                                    <input type="text" name="nama" class="form-control"
                                                                        id="exampleInput1"
                                                                        placeholder="nama_pembayaran" required>
                                                                </div>
                                                                <div class="form-group">
                                                                    <label for="exampleInputPassword1">Prodi</label>
                                                                    <select id="cars" class="form-control" name="prodi">
                                                                        @foreach($prodi as $item)
                                                                        <option value="{{$item->id}}">
                                                                            {{$item->nama_prodi}}</option>
                                                                        @endforeach
                                                                    </select>
                                                                </div>
                                                                <div class="form-group">
                                                                    <label for="exampleInput1">Semester</label>
                                                                    <input type="text" name="Angkatan"
                                                                        class="form-control" id="exampleInput1"
                                                                        placeholder="nama_pembayaran" required>
                                                                </div>
                                                                <div class="form-group">
                                                                    <label for="exampleInput1">Biaya</label>
                                                                    <input type="text" name="biaya" class="form-control"
                                                                        id="exampleInput1"
                                                                        placeholder="nama_pembayaran" required>
                                                                </div>
                                                            </div>
                                                            <!-- /.card-body -->
                                                            <div class="modal-footer justify-content-between">
                                                                <button type="button" class="btn btn-default"
                                                                    data-dismiss="modal">Close</button>
                                                                <button type="submit" class="btn btn-primary">Save
                                                                    changes</button>
                                                            </div>
                                                        </form>
                                                    </div>

                                                </div>
                                                <!-- /.modal-content -->
                                            </div>
                                            <!-- /.modal-dialog -->
                                        </div>
                                        <table class="table table-striped table-bordered table-sm"
                                            id="table-pembayaran">
                                            <thead>
                                                <tr>
                                                    <th>No</th>
                                                    <th>Daftar pembayaran</th>
                                                    <th>Jumlah pembayaran</th>
                                                    <th>Prodi</th>
                                                    <th>Semester</th>
                                                    <th>Action</th>
                                                </tr>
                                            </thead>
                                        </table>
                                    </div>
                                    <!-- /.card-body -->
                                </div>
                                <!-- /.card -->
                            </div>
                            <!-- /.col -->
                        </div>
                        <!-- /.row -->
                    </div>
                    <!-- /.container-fluid -->
                </section>

            </div>
            <!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <div class="modal fade" id="modal-edit-pembayaran">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Edit Data pembayaran</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="{{route('pembayaran.edit')}}" method="post">
                        @csrf

                        <div class="card-body">
                            <div class="form-group">
                                <label for="exampleInput1">Nama pembayaran</label>
                                <input type="text" name="nama" class="form-control" id="nama"
                                    placeholder="nama_pembayaran">
                            </div>
                            <div class="form-group">
                                <label for="exampleInputPassword1">Prodi</label>
                                <select id="prodi" class="form-control" name="prodi">
                                    @foreach($prodi as $item)
                                    <option value="{{$item->id}}">
                                        {{$item->nama_prodi}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="exampleInput1">Angkatan</label>
                                <input type="text" name="Angkatan" class="form-control" id="angkatan"
                                    placeholder="nama_pembayaran">
                            </div>
                            <div class="form-group">
                                <label for="exampleInput1">Biaya</label>
                                <input type="text" name="biaya" class="form-control" id="biaya"
                                    placeholder="nama_pembayaran">
                            </div>
                            <input type="hidden" name="id" id="id">
                        </div>
                        <!-- /.card-body -->
                        <div class="modal-footer justify-content-between">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary">Save
                                changes</button>
                        </div>
                    </form>
                </div>

            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <div class="modal fade" tabindex="-1" role="dialog" id="konfirmasi-modal" data-backdrop="false">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">PERHATIAN</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <p><b>Jika menghapus pembayaran maka</b></p>
                    <p>*data pembayaran tersebut hilang selamanya, apakah anda yakin?</p>
                </div>
                <div class="modal-footer bg-whitesmoke br">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-danger" name="tombol-hapus" id="tombol-hapus">Hapus
                        Data</button>
                </div>
            </div>
        </div>
    </div>
    @endsection

    @section('js')
    <script type="text/javascript">
    
    $('#tmbh').validate();
    $(function() {
        var oTable = $('#table-pembayaran').DataTable({
            serverSide: true,
            ajax: {
                url: '{{ url("pembayaran") }}'
            },
            columns: [{
                    data: 'DT_RowIndex',
                    name: 'DT_RowIndex'
                },
                {
                    data: 'daftar_pembayaran',
                    name: 'daftar_pembayaran'
                },
                {
                    data: 'biaya',
                    name: 'biaya'
                },
                {
                    data: 'nama_prodi',
                    name: 'nama_prodi'
                },
                {
                    data: 'angkatan',
                    name: 'angkatan'
                },
                {
                    data: 'action',
                    name: 'action'
                },
            ],
        });
    });
    $('body').on('click', '.edit-post', function() {
        var data_id = $(this).data('id');
        $.get('pembayaran/' + 'update/' + data_id, function(data) {
            console.log(data);
            $('#modal-edit-pembayaran').modal('show');
            //set value masing-masing id berdasarkan data yg diperoleh dari ajax get request diatas               
            $('#id').val(data.id);
            $('#nama').val(data.daftar_pembayaran);
            $('#biaya').val(data.biaya);
            $('#angkatan').val(data.angkatan);
        })
    });

    $(document).on('click', '.delete', function() {
        dataId = $(this).attr('id');
        $('#konfirmasi-modal').modal('show');
    });
    $('#tombol-hapus').click(function() {
        $.ajax({
            url: "pembayaran/delete/" + dataId, //eksekusi ajax ke url ini
            type: 'get',
            beforeSend: function() {
                $('#tombol-hapus').text('Hapus Data'); //set text untuk tombol hapus
            },
            success: function(data) { //jika sukses
                $('#konfirmasi-modal').modal('hide');
                location.reload();
            }
        })
    });
    </script>
    @endsection