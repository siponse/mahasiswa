@extends('layout')
@section('content')
<!-- Main content -->
<div class="content">
    <div class="container-fluid">
    <section class="content">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-12">
                                <div class="card">
                                    <div class="card-header">
                                        <h3 class="card-title">Data Mahasiswa</h3>
                                    </div>
                                    <!-- /.card-header -->
                                    <div class="card-body">
                                        <button type="button" class="btn btn-info" data-toggle="modal"
                                            data-target="#modal-lg">
                                            Tambah Mahasiswa
                                        </button>
                                        <div class="modal fade" id="modal-lg">
                                            <div class="modal-dialog modal-lg">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h4 class="modal-title">Tambah Mahasiswa</h4>
                                                        <button type="button" class="close" data-dismiss="modal"
                                                            aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                    <div class="modal-body">
                                                        <form action="{{route('mahasiswa.store')}}" method="post" id="tmbh">
                                                            @csrf
                                                            <div class="card-body">
                                                                <div class="form-group">
                                                                    <label for="exampleInput1">Nama Lengkap</label>
                                                                    <input type="text" name="nama" class="form-control"
                                                                        id="exampleInput1" placeholder="Nama lengkap" required>
                                                                </div>
                                                                <div class="form-group">
                                                                    <label for="exampleInputPassword1">VA</label>
                                                                    <input type="text" name="VA" class="form-control"
                                                                        id="exampleInputPassword1" placeholder="VA" required>
                                                                </div>
                                                                <div class="form-group">
                                                                    <label for="exampleInputPassword1">Prodi</label>
                                                                    <select id="cars" class="form-control" name="prodi">
                                                                        @foreach($prodi as $item)
                                                                        <option value="{{$item->id}}">
                                                                            {{$item->nama_prodi}}</option>
                                                                        @endforeach
                                                                    </select>
                                                                </div>
                                                                <div class="form-group">
                                                                    <label for="exampleInputPassword1">kelas</label>
                                                                    <input type="text" name="kelas" class="form-control"
                                                                        id="exampleInputPassword1" placeholder="Kelas" required>
                                                                </div>
                                                                <div class="form-group">
                                                                    <label for="exampleInputPassword1">angkatan</label>
                                                                    <input type="text" name="angkatan"
                                                                        class="form-control" id="exampleInputPassword1"
                                                                        placeholder="Angkatan" required>
                                                                </div>
                                                                <div class="form-group">
                                                                    <label for="exampleInputPassword1">Status</label>
                                                                    <select id="cars" class="form-control"
                                                                        name="beasiswa">
                                                                        @foreach($beasiswa as $item)
                                                                        <option value="{{$item->id}}">
                                                                            {{$item->nama_beasiswa}}</option>
                                                                        @endforeach
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <!-- /.card-body -->
                                                            <div class="modal-footer justify-content-between">
                                                                <button type="button" class="btn btn-default"
                                                                    data-dismiss="modal">Close</button>
                                                                <button type="submit" class="btn btn-primary">Save
                                                                    changes</button>
                                                            </div>
                                                        </form>
                                                    </div>

                                                </div>
                                                <!-- /.modal-content -->
                                            </div>
                                            <!-- /.modal-dialog -->
                                        </div>
                                        <table class="table table-striped table-bordered table-sm" id="table-mahasiswa">
                                            <thead>
                                                <tr>
                                                    <th>No</th>
                                                    <th>Nama</th>
                                                    <th>VA</th>
                                                    <th>prodi</th>
                                                    <th>angkatan</th>
                                                    <th>status</th>
                                                    <th>Action</th>
                                                </tr>
                                            </thead>
                                        </table>
                                    </div>
                                    <!-- /.card-body -->
                                </div>
                                <!-- /.card -->
                            </div>
                            <!-- /.col -->
                        </div>
                        <!-- /.row -->
                    </div>
                    <!-- /.container-fluid -->
                </section><!-- /.container-fluid -->
    </div>
    <div class="modal fade" id="modal-edit-mahasiswa">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Edit Data Mahasiswa</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="{{route('mahasiswa.edit')}}" method="post">
                        @csrf
                        <div class="card-body">
                            <div class="form-group">
                                <label for="exampleInput1">Nama Lengkap</label>
                                <input type="text" name="nama" class="form-control" id="nama"
                                    placeholder="Nama lengkap">
                            </div>
                            <div class="form-group">
                                <label for="exampleInputPassword1">VA</label>
                                <input type="text" name="VA" class="form-control" id="VA"
                                    placeholder="NIM">
                            </div>
                            <div class="form-group">
                                <label for="exampleInputPassword1">Prodi</label>
                                <select id="cars" class="form-control" name="prodi">
                                    @foreach($prodi as $item)
                                    <option value="{{$item->id}}">
                                        {{$item->nama_prodi}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="exampleInputPassword1">kelas</label>
                                <input type="text" name="kelas" class="form-control" id="kelas"
                                    placeholder="NIM">
                            </div>
                            <div class="form-group">
                                <label for="exampleInputPassword1">angkatan</label>
                                <input type="text" name="angkatan" class="form-control" id="angkatan"
                                    placeholder="NIM">
                            </div>
                            <div class="form-group">
                                <label for="exampleInputPassword1">Status</label>
                                <select id="cars" class="form-control" name="status">
                                    @foreach($beasiswa as $item)
                                    <option value="{{$item->id}}">
                                        {{$item->nama_beasiswa}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <input type="hidden" name="id" id="id">
                        </div>
                        <!-- /.card-body -->
                        <div class="modal-footer justify-content-between">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary">Save
                                changes</button>
                        </div>
                    </form>
                </div>

            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <div class="modal fade" tabindex="-1" role="dialog" id="konfirmasi-modal" data-backdrop="false">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">PERHATIAN</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <p><b>Jika menghapus Mahasiswa maka</b></p>
                    <p>*data mahasiswa tersebut hilang selamanya, apakah anda yakin?</p>
                </div>
                <div class="modal-footer bg-whitesmoke br">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-danger" name="tombol-hapus" id="tombol-hapus">Hapus
                        Data</button>
                </div>
            </div>
        </div>
    </div>
    @endsection

    @section('js')
    <script type="text/javascript">
    $('#tmbh').validate();
    $(function() {
        var oTable = $('#table-mahasiswa').DataTable({
            serverSide: true,
            ajax: {
                url: '{{ url("mahasiswa") }}'
            },
            columns: [{
                    data: 'DT_RowIndex',
                    name: 'DT_RowIndex'
                },
                {
                    data: 'Nama',
                    name: 'Nama'
                },
                {
                    data: 'VA',
                    name: 'VA'
                },
                {
                    data: 'nama_prodi',
                    name: 'nama_prodi'
                },
                {
                    data: 'Angkatan',
                    name: 'Angkatan'
                },
                {
                    data: 'nama_beasiswa',
                    name: 'nama_beasiswa'
                },
                {
                    data: 'action',
                    name: 'action'
                },
            ],
        });
    });
    $('body').on('click', '.edit-post', function() {
        var data_id = $(this).data('id');
        $.get('mahasiswa/' + 'update/' + data_id, function(data) {
            console.log(data_id);
            $('#modal-edit-mahasiswa').modal('show');
            //set value masing-masing id berdasarkan data yg diperoleh dari ajax get request diatas               
            $('#id').val(data.id);
            $('#nama').val(data.Nama);
            $('#VA').val(data.VA);
            $('#prodi').val(data.nama_prodi);
            $('#kelas').val(data.Kelas);
            $('#angkatan').val(data.Angkatan);
        })
    });

    $(document).on('click', '.delete', function() {
        dataId = $(this).attr('id');
        $('#konfirmasi-modal').modal('show');
    });
    $('#tombol-hapus').click(function() {
        $.ajax({
            url: "mahasiswa/delete/" + dataId, //eksekusi ajax ke url ini
            type: 'get',
            beforeSend: function() {
                $('#tombol-hapus').text('Hapus Data'); //set text untuk tombol hapus
            },
            success: function(data) { //jika sukses

                $('#konfirmasi-modal').modal('hide');
                location.reload();
            }
        })
    });
    </script>
    @endsection