<!DOCTYPE html>
<html>

<head>
    <title>Data Keuangan Mahasiswa</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
        integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<style>
.bid{
	margin-bottom:30px;
}
.bid td{
	padding-bottom:10px;
	padding-top:30px;
}
header{
    position: relative;
    display: block;
    width: 90%;
    height: 120px;
  }
.judul{
    margin-left:25%;
}
.judul1{
    padding-top:80px;
    margin-left:25%;
}
hr{
    color:black;
}
</style>

</head>

<body>
<header class="onlyprint">
  <div class="row">
        <div class="image">
            <img src="{{public_path('logo.png')}}" style="width:200px;" alt="" srcset="">
        </div>
        <div class="judul"><center><h3>SEKOLAH TINGGI ISLAM AL MUHAMMAD</h3></center></div>
        <div class="judul1"><center><h5>CEPU</h5></center></div>
  </div>
</header>
<hr>
    <div class="container">
        <center>
            <h4>Data Tanggungan Mahasiswa</h4>
        </center>
        <br />
        <table class="bid">
            <tr>
                <td>Nama</td>
                <td>:</td>
                <td> {{$post[0]->Nama}}</td>
            </tr>
            <tr>
                <td>NIM </td>
                <td>:</td>
                <td> {{$post[0]->VA}}</td>
            </tr>
            <tr>
                <td>Jurusan </td>
                <td>:</td>
                <td> {{$post[0]->nama_prodi}}</td>
            </tr>
            <tr>
                <td>Angkatan </td>
                <td>:</td>
                <td> {{$post[0]->Angkatan}}</td>
            </tr>
            <tr>
                <td>Semester </td>
                <td>:</td>
                <td>{{$semester}}</td>
            </tr>
        </table>
        <table class='table table-bordered'>
            <thead>
                <tr>
                    <th>No</th>
                    <th>Daftar Tanggungan</th>
                    <th>Biaya</th>
                    <th>Jumlah yang di bayar</th>
                    <th>Status</th>
                </tr>
            </thead>
            <tbody>
                <?php
			$no=1;
			
			?>
                @foreach($post as $item)
                <tr>
                    <td>{{$no++}}</td>
                    <td>{{$item->daftar_pembayaran}}</td>
                    <td>{{$item->biaya}}</td>
                    <td>{{$item->jumlah}}</td>
                    <?php
					if ($item->jumlah == null) {
						$status = "belum lunas";
					}else {
						$status="lunas";
					}?>
                    <td>{{$status}}</td>
                </tr>
                @endforeach
            </tbody>
        </table>

    </div>

</body>

</html>