<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class pembayaran extends Model
{
    protected $fillable = [
        'mahasiswa_id', 'total','jumlah','status'
    ];

    public function detail_pembayaran()
    {
        return $this->hasMany('App\detail_pembayaran');
    }
}
