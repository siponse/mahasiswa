<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\prodi;
use App\beasiswa;
use App\pembayaran;
use App\mahasiswa;
use App\rincian;
use DB;
class viewController extends Controller
{
    public function Mahasiswa(Request $request)
    {
        $prodi = prodi::get();
        $beasiswa = beasiswa::get();
        
        return view('starter')->withbeasiswa($beasiswa)->withprodi($prodi);
    }
    public function dasbor(Request $request)
    {
        $mahasiswa = mahasiswa::get();
        $post = DB::table('rincian_mahasiswa')
        ->sum('biaya');
        $total=intval($post);
        
        $data = DB::table('smt1s')
        ->first();
        return view('welcome')->withmahasiswa($mahasiswa)->withtotal($total)->withdata($data);
    }

    public function pembayaran(Request $request)
    {
        $prodi = prodi::get();
        $mahasiswa = mahasiswa::get();
        $rincian = rincian::get();
        
        return view('pembayaran')->withprodi($prodi)->withmahasiswa($mahasiswa)->withrincian($rincian);
    }
    public function keuangan(Request $request)
    {
        $prodi = prodi::get();
        $mahasiswa = mahasiswa::get();
        $rincian = rincian::get();
        
        return view('keuangan')->withprodi($prodi)->withmahasiswa($mahasiswa)->withrincian($rincian);
    }
   
}
