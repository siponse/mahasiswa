<?php

namespace App\Http\Controllers\Data;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\pembayaran;
use App\prodi;
use App\mahasiswa;
use App\rincian;
use DB;
use Yajra\Datatables\Datatables;

class KeuanganController extends Controller
{
    
    public function keuangan(Request $request)
    {
        $prodi = prodi::get();
        $mahasiswa = mahasiswa::get();
        $rincian = rincian::get();
        $pembayaran = DB::table('mahasiswas')
        ->join('prodis', 'mahasiswas.id_prodi', '=', 'prodis.id')
        ->select('mahasiswas.id','mahasiswas.Nama','VA','nama_prodi','mahasiswas.semester','mahasiswas.Angkatan')
        ->get();    
        // dd($pembayaran);
        if($request->ajax()){
            return datatables()->of($pembayaran)
            ->addColumn('keterangan', function($data){
                $button = '<a href="javascript:void(0)" data-toggle="tooltip"  data-id="'.$data->id.'" data-original-title="detail" class="detail btn btn-info btn-sm detail-post">Lihat Detail</a>';     
                return $button;
            })
            ->addColumn('smt', function($data){
                $b=idate('Y');
                $thn=$data->Angkatan;
                $dt=explode("/",$thn);
                $sm=DB::select("SELECT sms FROM `smt1s`");;
                $test=intval($dt[0]);
                $ac=$b-$test;
                if($sm[0]->sms==1){
                    $semester=$ac+$ac;
                }else{
                    $semester=$ac+$ac+1;
                }
                $smt = '<td>'.$semester.'</td>';     
                return $smt;
            })
                        ->addColumn('action', function($data){
                            $button = '<a href="javascript:void(0)" data-toggle="tooltip"  data-id="'.$data->id.'" data-original-title="Edit" class="edit btn btn-info btn-sm edit-post"><i class="far fa-edit"></i> Edit</a>';
                            $button .= '&nbsp;&nbsp;';
                            $button .= '<button type="button" name="delete" id="'.$data->id.'" class="delete btn btn-danger btn-sm"><i class="far fa-trash-alt"></i> Delete</button>';     
                            return $button;
                        })
                        ->rawColumns(['action','keterangan','smt'])
                        ->addIndexColumn()
                        ->make(true);
        }
        // dd($keuangan);
        return view('keuangan')->withprodi($prodi)->withmahasiswa($mahasiswa)->withrincian($rincian);

    }
    public function store(Request $request)
    {
        
        foreach ($request->mahasiswa as $key) {
            // var_dump($key);
            $data=pembayaran::create([
                "mahasiswa_id"=>$key,
                "total"=>$request->Total,
                "jumlah"=>$request->biaya,
                "status"=>1,
                ]); 
                
            foreach ($request->rincian as $item) {
               
                    $data->detail_pembayaran()->create([
                    "rincian_id"=>$item,
                    ]);       
            }
        }
            
            
        return back();
    }
    public function show($id)
    {
        // DD($id);
        $where = array('id' => $id);
        $post = DB::table('mahasiswas')
        ->join('pembayarans','mahasiswas.id','=','pembayarans.mahasiswa_id')
        ->join('prodis', 'mahasiswas.id_prodi', '=', 'prodis.id')
        ->join('detail_pembayarans','detail_pembayarans.pembayaran_id','=','pembayarans.id')
        ->join('rincians', 'detail_pembayarans.rincian_id', '=', 'rincians.id')
        ->where('mahasiswas.id','=',$id)
        ->get();
        $tanggung=DB::table('tanggungan')
        ->join('rincians', 'tanggungan.rincian_id', '=', 'rincians.id')
        ->where('mahasiswa_id','=',$id)
        ->get();
        return response()->json(['post'=>$post,'tanggung'=>$tanggung]);
    }
    public function showharga($id)
    {
        $tes = explode(',', $id);
        $post  = rincian::find($tes);
        return response()->json($post);
        
    }
    public function edit(Request $request)
    {   
        $post  = rincian::find($request->id);
        $post->update([
           'daftar_pembayaran'=> $request->nama,
           'biaya'=>$request->biaya,
           'angkatan'=>$request->Angkatan,
           'prodi_id'=>$request->prodi 
        ]);
        return back();
    }
    public function delete($id)
    {
        $post = pembayaran::where('id',$id)->delete();
     
        return response()->json($post);
    }
}