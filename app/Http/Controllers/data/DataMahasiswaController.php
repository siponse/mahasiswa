<?php

namespace App\Http\Controllers\data;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\mahasiswa;
use App\prodi;
use App\beasiswa;
use Yajra\Datatables\Datatables;
use DB;

class DataMahasiswaController extends Controller
{
    public function Mahasiswa(Request $request)
    {
        $mahasiswa = DB::table('mahasiswas')
        ->join('beasiswas', 'mahasiswas.id_beasiswa', '=', 'beasiswas.id')
        ->join('prodis', 'mahasiswas.id_prodi', '=', 'prodis.id')
        ->select('mahasiswas.id','Nama','VA','Kelas','Angkatan','nama_prodi','nama_beasiswa')
        ->get();
        // dd($mahasiswa);
        if($request->ajax()){
            return datatables()->of($mahasiswa)
                        ->addColumn('action', function($data){
                            $button = '<a href="javascript:void(0)" data-toggle="tooltip"  data-id="'.$data->id.'" data-original-title="Edit" class="edit btn btn-info btn-sm edit-post"><i class="far fa-edit"></i> Edit</a>';
                            $button .= '&nbsp;&nbsp;';
                            $button .= '<button type="button" name="delete" id="'.$data->id.'" class="delete btn btn-danger btn-sm"><i class="far fa-trash-alt"></i> Delete</button>';     
                            return $button;
                        })
                        ->rawColumns(['action'])
                        ->addIndexColumn()
                        ->make(true);
        }
    }
    public function store(Request $request)
    {
        $data= new mahasiswa;
        $data->nama=$request->nama;
        $data->VA=$request->VA;
        $data->id_prodi=$request->prodi;
        $data->Kelas=$request->kelas;
        $data->Angkatan=$request->angkatan;
        $data->id_beasiswa=$request->beasiswa;
        $data->save();
        return back();
    }
    public function show($id)
    {
        $where = array('id' => $id);
        $post  = mahasiswa::where($where)->first();
        return response()->json($post);
    }
    public function edit(Request $request)
    {
        $post  = mahasiswa::find($request->id);
        $prodi  = prodi::find($post->id_prodi);
        $beasiswa  = beasiswa::find($post->id_beasiswa);
        // dd($request->all());
        $post->update([
           'Nama'=> $request->nama,
           'VA'=>$request->VA,
           'Kelas'=>$request->kelas,
           'Angkatan'=>$request->angkatan,
           'id_beasiswa'=>$request->status,
           'id_prodi'=>$request->prodi
        ]);
        return back();
    }
    public function delete($id)
    {
        $post = mahasiswa::where('id',$id)->delete();
     
        return response()->json($post);
    }
}
