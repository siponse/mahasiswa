<?php

namespace App\Http\Controllers\data;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use App\smt1;
use PDF;

class DataController extends Controller
{
    public function daftar($id)
    {
        $where = array('id' => $id);
        $post = DB::table('rincians ')
        ->join('mahasiswas','mahasiswas.Angkatan','=','rincians.angkatan')
        ->select('rincians.id','daftar_pembayaran as text')
        ->where('mahasiswa','=',$id)
        ->get();
        return response()->json($post);
    }
    public function cetak_pdf(Request $request)
    {
        $where = array('id' => $request->id);
        $post = DB::select("SELECT mahasiswas.id,mahasiswas.Nama,mahasiswas.Angkatan,rincians.daftar_pembayaran,mahasiswas.VA,rincians.biaya,pembayarans.jumlah ,detail_pembayarans.id,pembayarans.id,nama_prodi FROM mahasiswas inner JOIN rincians ON mahasiswas.Angkatan=rincians.angkatan inner JOIN prodis ON prodis.id=mahasiswas.id_prodi left JOIN (detail_pembayarans inner JOIN pembayarans  ON pembayarans.id=detail_pembayarans.pembayaran_id) ON pembayarans.mahasiswa_id=mahasiswas.id AND detail_pembayarans.rincian_id=rincians.id WHERE mahasiswas.id = $request->id");
        // DD($post[0]->Nama);
        $b=idate('Y');
		$thn=$post[0]->Angkatan;
        $dt=explode("/",$thn);
        $sm=DB::select("SELECT sms FROM `smt1s`");;
        $test=intval($dt[0]);
        $ac=$b-$test;
        if($sm==1){
            $semester=$ac+$ac;
        }else{
            $semester=$ac+$ac+1;
        }
	    $pdf = PDF::loadview('print',['post'=>$post,'semester'=>$semester]);
        return $pdf->download('laporan-keuangan-pdf');
    //    return view('print',['post'=>$post,'semester'=>$semester]);
    }
    public function sms($id)
    {
        $post  = smt1::find(1);
        $post->update([
           'sms'=> $id,
        ]);
        return response()->json($post);
    }
}