<?php

namespace App\Http\Controllers\Data;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\pembayaran;
use App\prodi;
use App\rincian;
use DB;
use Yajra\Datatables\Datatables;

class PembayaranController extends Controller
{
    
    public function pembayaran(Request $request)
    {
        $prodi = prodi::get();
        $pembayaran = DB::table('rincians')
       ->join('prodis', 'rincians.prodi_id', '=', 'prodis.id')
       ->select('rincians.id','daftar_pembayaran','biaya','nama_prodi','angkatan')
       ->get();
        if($request->ajax()){
            return datatables()->of($pembayaran)
                        ->addColumn('action', function($data){
                            $button = '<a href="javascript:void(0)" data-toggle="tooltip"  data-id="'.$data->id.'" data-original-title="Edit" class="edit btn btn-info btn-sm edit-post"><i class="far fa-edit"></i> Edit</a>';
                            $button .= '&nbsp;&nbsp;';
                            $button .= '<button type="button" name="delete" id="'.$data->id.'" class="delete btn btn-danger btn-sm"><i class="far fa-trash-alt"></i> Delete</button>';     
                            return $button;
                        })
                        ->rawColumns(['action'])
                        ->addIndexColumn()
                        ->make(true);
        }
        // dd($pembayaran);
        return view('rincian')->withprodi($prodi);

    }
    public function store(Request $request)
    {
        $data= new rincian;
        $data->daftar_pembayaran=$request->nama;
        $data->prodi_id=$request->prodi;
        $data->angkatan=$request->Angkatan;
        $data->biaya=$request->biaya;
        $data->save();
        return back();
    }
    public function show($id)
    {
        $where = array('id' => $id);
        $post  = rincian::where($where)->first();
        return response()->json($post);
    }
    public function edit(Request $request)
    {   
        $post  = rincian::find($request->id);
        $post->update([
           'daftar_pembayaran'=> $request->nama,
           'biaya'=>$request->biaya,
           'angkatan'=>$request->Angkatan,
           'prodi_id'=>$request->prodi 
        ]);
        return back();  
    }
    public function delete($id)
    {
        $post = rincian::where('id',$id)->delete();
    
        return response()->json($post);
    }
}
