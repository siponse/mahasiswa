<?php

namespace App\Http\Controllers\Data;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\beasiswa;
use Yajra\Datatables\Datatables;

class BeasiswaController extends Controller
{
    
    public function beasiswa(Request $request)
    {
        $beasiswa = beasiswa::all();
        if($request->ajax()){
            return datatables()->of($beasiswa)
                        ->addColumn('action', function($data){
                            $button = '<a href="javascript:void(0)" data-toggle="tooltip"  data-id="'.$data->id.'" data-original-title="Edit" class="edit btn btn-info btn-sm edit-post"><i class="far fa-edit"></i> Edit</a>';
                            $button .= '&nbsp;&nbsp;';
                            $button .= '<button type="button" name="delete" id="'.$data->id.'" class="delete btn btn-danger btn-sm"><i class="far fa-trash-alt"></i> Delete</button>';     
                            return $button;
                        })
                        ->rawColumns(['action'])
                        ->addIndexColumn()
                        ->make(true);
        }

        return view('beasiswa');

    }
    public function store(Request $request)
    {
        $data= new beasiswa;
        $data->nama_beasiswa=$request->nama;
        $data->jumlah_beasiswa=$request->jumlah;
        $data->masa_beasiswa=$request->masa;
        $data->save();
        return back();
    }
    public function show($id)
    {
        $where = array('id' => $id);
        $post  = beasiswa::where($where)->first();
        return response()->json($post);
    }
    public function edit(Request $request)
    {   
        $post  = beasiswa::find($request->id);
        $post->update([
           'nama_beasiswa'=> $request->nama,
           'jumlah_beasiswa'=>$request->jumlah,
           'masa_beasiswa'=>$request->masa 
        ]);
        return back();
    }
    public function delete($id)
    {
        $post = beasiswa::where('id',$id)->delete();
     
        return response()->json($post);
    }
}
