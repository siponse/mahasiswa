<?php

namespace App\Http\Controllers\Data;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\prodi;
use App\fakultas;
use DB;
use Yajra\Datatables\Datatables;

class ProdiController extends Controller
{
    
    public function prodi(Request $request)
    {
        $fakultas = fakultas::get();
        $prodi = DB::table('prodis')
       ->join('fakultas', 'prodis.fakultas_id', '=', 'fakultas.id')
       ->select('prodis.id','nama_prodi','nama_fakultas')
       ->get();
        if($request->ajax()){
            return datatables()->of($prodi)
                        ->addColumn('action', function($data){
                            $button = '<a href="javascript:void(0)" data-toggle="tooltip"  data-id="'.$data->id.'" data-original-title="Edit" class="edit btn btn-info btn-sm edit-post"><i class="far fa-edit"></i> Edit</a>';
                            $button .= '&nbsp;&nbsp;';
                            $button .= '<button type="button" name="delete" id="'.$data->id.'" class="delete btn btn-danger btn-sm"><i class="far fa-trash-alt"></i> Delete</button>';     
                            return $button;
                        })
                        ->rawColumns(['action'])
                        ->addIndexColumn()
                        ->make(true);
        }
        // dd($prodi);
        return view('prodi')->withfakultas($fakultas);

    }
    public function store(Request $request)
    {
        $data= new prodi;
        $data->nama_prodi=$request->nama;
        $data->fakultas_id=$request->fakultas_id;
        $data->save();
        return back();
    }
    public function show($id)
    {
        $where = array('id' => $id);
        $post  = prodi::where($where)->first();
        return response()->json($post);
    }
    public function edit(Request $request)
    {   
        $post  = prodi::find($request->id);
        $post->update([
           'nama_prodi'=> $request->nama,
           'jumlah_prodi'=>$request->jumlah,
           'masa_prodi'=>$request->masa 
        ]);
        return back();
    }
    public function delete($id)
    {
        $post = prodi::where('id',$id)->delete();
     
        return response()->json($post);
    }
}
