<?php

namespace App\Http\Controllers\Data;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\fakultas;
use Yajra\Datatables\Datatables;

class FakultasController extends Controller
{
    
    public function fakultas(Request $request)
    {
        $fakultas = fakultas::all();
        if($request->ajax()){
            return datatables()->of($fakultas)
                        ->addColumn('action', function($data){
                            $button = '<a href="javascript:void(0)" data-toggle="tooltip"  data-id="'.$data->id.'" data-original-title="Edit" class="edit btn btn-info btn-sm edit-post"><i class="far fa-edit"></i> Edit</a>';
                            $button .= '&nbsp;&nbsp;';
                            $button .= '<button type="button" name="delete" id="'.$data->id.'" class="delete btn btn-danger btn-sm"><i class="far fa-trash-alt"></i> Delete</button>';     
                            return $button;
                        })
                        ->rawColumns(['action'])
                        ->addIndexColumn()
                        ->make(true);
        }
        return view('fakultas');

    }
    public function store(Request $request)
    {
        $data= new fakultas;
        $data->nama_fakultas=$request->nama;
        $data->save();
        return back();
    }
    public function show($id)
    {
        $where = array('id' => $id);
        $post  = fakultas::where($where)->first();
        return response()->json($post);
    }
    public function edit(Request $request)
    {   
        $post  = fakultas::find($request->id);
        $post->update([
           'nama_fakultas'=> $request->nama,
        ]);
        return back();
    }
    public function delete($id)
    {
        $post = fakultas::where('id',$id)->delete();
     
        return response()->json($post);
    }
}
