<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class detail_pembayaran extends Model
{

    protected $fillable = [
        'pembayaran_id','rincian_id'
    ];
    public function pembayaran()
    {
    return $this->belongsTo(pembayaran::class,'pembayaran_id');
    }
}
