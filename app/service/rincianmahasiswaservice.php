<?php

namespace App\Services\Santri;

use App\rincian;
use App\mahasiswa;

class ShowSantriService implements ShowUserInterface
{
    /**
     * The model of object.
     *
     * @var \App\Models\Santri
     */
    protected $model;

    /**
     * The model of object.
     *
     * @var \App\Models\Santri
     */
    protected $santri;

    /**
     * The selection columns.
     *
     * @var array
     */
    protected $columns;

    /**
     * The relations of santri.
     *
     * @var array
     */
    protected $relations;

    /**
     * Constructor.
     * 
     * @param void
     */
    public function __construct()
    {
        $this->model = new Santri();
        $this->columns = ['*'];
        $this->relations = [];
    }

    /**
     * Set selection columns.
     *
     * @param array $columns
     * @return ShowSantriService
     */
    public function setColumns(array $columns)
    {
        $this->columns = $columns;

        return $this;
    }

    /**
     * Set relations of santri.
     *
     * @param array $relations
     * @return ShowSantriService
     */
    public function setRelations(array $relations)
    {
        $this->relations = $relations;

        return $this;
    }

    /**
     * Find the santri.
     *
     * @param string $column
     * @param int|string|array $value
     * @param string $operator
     * @return ShowSantriService
     */
    public function search(string $column, $value, string $operator = '=')
    {
        $this->santri = $this->model->with($this->relations)
                                    ->select($this->columns)
                                    ->where($column, $operator, $value);

        return $this;
    }

    /**
     * Get the santri.
     *
     * @param void
     * @return \App\Models\Santri
     */
    public function get()
    {
        return $this->santri->first();
    }
}
