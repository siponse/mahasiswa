<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class beasiswa extends Model
{
    protected $fillable = [
        'nama_beasiswa','jumlah_beasiswa', 'masa_beasiswa'
    ];
}
