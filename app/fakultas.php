<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class fakultas extends Model
{
    protected $fillable = [
        'nama_fakultas'
    ];
    public function prodi()
    {
        return $this->hasMany(prodi::class);
    }

}
