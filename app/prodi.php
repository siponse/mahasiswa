<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class prodi extends Model
{
    protected $fillable = [
        'fakultas_id', 'nama_prodi'
    ];
    public function fakultas()
    {
    return $this->belongsTo(fakultas::class);
    }
}