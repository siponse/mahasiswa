<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class rincian extends Model
{
    protected $fillable = [
        'daftar_pembayaran', 'prodi_id','biaya','angkatan'
    ];
}
