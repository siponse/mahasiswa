<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSmt1sTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('smt1s', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('mahasiswa_id');
            $table->Integer('Jumlah_pembayaran');
            $table->Integer('Daftar_ulang');
            $table->Integer('SPP');
            $table->Integer('UTS');
            $table->Integer('UAS');
            $table->Integer('Total_pembayaran');
            $table->Integer('Kekurangan');
            $table->Integer('semester');
            $table->timestamps();


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('smt1s');
    }
}
