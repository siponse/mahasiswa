<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/dashboard','ViewController@dasbor')->name('dashboard');

Route::get('/','ViewController@Mahasiswa')->name('mahasiswa');
Route::get('/mahasiswa','Data\DataMahasiswaController@Mahasiswa')->name('mahasiswa');
Route::post('/mahasiswa/post','Data\DataMahasiswaController@store')->name('mahasiswa.store');
Route::get('/mahasiswa/update/{id}','Data\DataMahasiswaController@show')->name('mahasiswa.show');
Route::post('/mahasiswa/edit','Data\DataMahasiswaController@edit')->name('mahasiswa.edit');
Route::get('/mahasiswa/delete/{id}','Data\DataMahasiswaController@delete')->name('mahasiswa.delete');


Route::get('/beasiswa','Data\BeasiswaController@beasiswa')->name('beasiswa');
Route::post('/beasiswa/post','Data\BeasiswaController@store')->name('beasiswa.store');
Route::get('/beasiswa/update/{id}','Data\BeasiswaController@show')->name('beasiswa.show');
Route::post('/beasiswa/edit','Data\BeasiswaController@edit')->name('beasiswa.edit');
Route::get('/beasiswa/delete/{id}','Data\BeasiswaController@delete')->name('beasiswa.delete');

Route::get('/fakultas','Data\FakultasController@fakultas')->name('fakultas');
Route::post('/fakultas/post','Data\FakultasController@store')->name('fakultas.store');
Route::get('/fakultas/update/{id}','Data\FakultasController@show')->name('fakultas.show');
Route::post('/fakultas/edit','Data\FakultasController@edit')->name('fakultas.edit');
Route::get('/fakultas/delete/{id}','Data\FakultasController@delete')->name('fakultas.delete');

Route::get('/prodi','Data\ProdiController@prodi')->name('prodi');
Route::post('/prodi/post','Data\ProdiController@store')->name('prodi.store');
Route::get('/prodi/update/{id}','Data\ProdiController@show')->name('prodi.show');
Route::post('/prodi/edit','Data\ProdiController@edit')->name('prodi.edit');
Route::get('/prodi/delete/{id}','Data\ProdiController@delete')->name('prodi.delete');

Route::get('/pembayaran','Data\PembayaranController@pembayaran')->name('pembayaran');
Route::post('/pembayaran/post','Data\PembayaranController@store')->name('pembayaran.store');
Route::get('/pembayaran/update/{id}','Data\PembayaranController@show')->name('pembayaran.show');
Route::post('/pembayaran/edit','Data\PembayaranController@edit')->name('pembayaran.edit');
Route::get('/pembayaran/delete/{id}','Data\PembayaranController@delete')->name('pembayaran.delete');


Route::get('/bayar','ViewController@pembayaran')->name('bayar');
Route::get('/pembayaranmahasiswa','Data\PembayaranMahasiswaController@pembayaranmahasiswa')->name('pembayaranmahasiswa');
Route::post('/pembayaranmahasiswa/post','Data\PembayaranMahasiswaController@store')->name('pembayaranmahasiswa.store');
Route::get('/pembayaranmahasiswa/update/{id}','Data\PembayaranMahasiswaController@show')->name('pembayaranmahasiswa.show');
Route::get('/pembayaranmahasiswa/showharga/{id}','Data\PembayaranMahasiswaController@showharga')->name('pembayaranmahasiswa.showharga');
Route::post('/pembayaranmahasiswa/edit','Data\PembayaranMahasiswaController@edit')->name('pembayaranmahasiswa.edit');
Route::get('/pembayaranmahasiswa/delete/{id}','Data\PembayaranMahasiswaController@delete')->name('pembayaranmahasiswa.delete');


Route::get('/uang','viewController@keuangan')->name('uang');
Route::get('/keuangan','Data\KeuanganController@keuangan')->name('keuangan');
Route::post('/keuangan/post','Data\KeuanganController@store')->name('keuangan.store');
Route::get('/keuangan/update/{id}','Data\KeuanganController@show')->name('keuangan.show');
Route::get('/keuangan/showharga/{id}','Data\KeuanganController@showharga')->name('keuangan.showharga');
Route::post('/keuangan/edit','Data\KeuanganController@edit')->name('keuangan.edit');
Route::get('/keuangan/delete/{id}','Data\KeuanganController@delete')->name('keuangan.delete');

Route::post('/data/cetak_pdf', 'Data\DataController@cetak_pdf')->name('cetak');
Route::get('/data/daftar/{id}','Data\DataController@daftar')->name('daftar');
Route::get('/sms/{id}','Data\DataController@sms')->name('sms');